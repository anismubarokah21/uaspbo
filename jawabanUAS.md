# Nomor 1

Mampu menunjukkan keseluruhan Use Case beserta ranking dari tiap Use Case dari produk digital: 

Use case user
Use case manajemen perusahaan
Use case direksi perusahaan (dashboard, monitoring, analisis)

Use Case User
| NO  |  Use case | Bisa |Peringkat |
| --- | ------| ---- |--------- |
|  1  | User  | Login dalam ke akun      |     1     |
|  2  | User  | Logout dari akun     |    2      |
|  3  | User  | Memesan Tiket     |     3     |
|  4  | User  | Mengetahui jadwal keberangkatan      |    4      |
|  5  | User  | Melihat harga tiket     |     5     |
|  6  | User  | Memilih tempat duduk     |     6     |
|  7  | User  | Melakukan pembayaran tiket     |     7     |
|  8  | User  | Menyimpan data pemesanan tiket     |    8      |
|  9  | User  | Melakukan refund tiket     |     9     |
|  10 | User  | Mengetahui informasi terbaru tentang kereta api     |  10        |
|  11 | User  | Melihat kereta api yang beroperasi      |   11       |
|  12 | User  | Memantau status pemesanan tiket     |    12      |
|  13 | User  | Mengakses informasi tentang stasiun kereta api     |   13       |
|  14 | User  | Menghubungi customer service KAI      |     14     |
|  15 | User  | Mengakses promo dan diskon     |     15     |
|  16 | User  | Menyimpan riwayat perjalanan     |   16       |
|  17 | User  | Memanfaatkan fitur QR code     |    17      |
|  18 | User  | Mencari kereta berdasarkan stasiun asal dan tujuan     |  18        |
|  19 | User  | Melihat profil pengguna      |   19       |
|  20 | User  | Mengubah atau memperbarui profil pengguna      | 20         |
|  21 | User  | Menghubungi layanan pelanggan      | 21         |
|  22 | User  | Memberikan umpan balik dan rating pada aplikasi KAI Access     |    22      |
|  23 | Manajemen  | Manajemen Penumpang     |      1    |
|  24 | Manajemen  | Manajemen Ketersediaan Kursi      |     2     |
|  25 | Manajemen  | Pembayaran dan Keuangan     |     3     |
|  26 | Manajemen  | Analisis Data Penjualan      |     4     |
|  27 | Manajemen  | Layanan Pelanggan     |    5      |
|  28 | Manajemen  | Melihat laporan penjualan tiket kereta api     |  6        |
|  29 | Manajemen      | Membuat dan mengelola promosi atau penawaran khusus     |   7       |
|  30 | Manajemen      | Mengelola sistem pembayaran     |    8      |
|  31 | Manajemen      | Mengelola daftar pengguna yang terdaftar dalam aplikasi     |     9     |
|  32 | Manajemen      | Mengelola data pengguna     |    10      |
|  33 | Manajemen      | Mengelola keamanan aplikasi     |     11     |
|  34 | Manajemen      | Mengelola kebijakan refund dan pembatalan     |  12        |
|  35 | Manajemen      | Mengelola notifikasi kepada pengguna     |    13      |
|  36 | Manajemen      | Mengelola layanan pelanggan     |     14     |
|  37 | Manajemen      | Mengelola profil aplikasi     |    15      |
|  38 | Manajemen      | Mengatur dan mengelola izin akses pengguna     |    16      |
|  39 | Manajemen      | Melakukan pemeliharaan sistem     |  17        |
|  40 | Direksi | Dashboard Kinerja     |      1    |
|  41 | Direksi | Monitoring Operasional     |     2     |
|  42 | Direksi | Analisis Kinerja      |   3       |
|  43 | Direksi | Pengelolaan Aset      |     4     |
|  44 | Direksi | Pelaporan dan Keuangan     |     5     |
|  45 | Direksi | Melihat daftar pengguna yang terdaftar dalam aplikasi     |   6       |
|  46 | Direksi | Melihat laporan penjualan tiket kereta api     |  7        |
|  47 | Direksi | Membuat dan mengelola promosi atau penawaran khusus     |     8     |
|  48 | Direksi | Mengelola sistem pembayaran     |   9       |
|  49 | Direksi | Mengelola notifikasi kepada pengguna      |   10       |
|  50 | Direksi | Mengelola profil aplikasi     |     11     |
|  51 | Direksi | Mengatur dan mengelola izin akses pengguna     |    12      |
|  52 | Direksi | Mengelola layanan pelanggan      |  13        |
|  53 | Direksi      | Mengelola data pengguna      |    14      |
|  54 | Direksi      | Mengelola keamanan aplikasi      |    15      |
|  56 | Direksi      | Mengelola kebijakan refund dan pembatalan      |    16      |
|  57 | Direksi      | Mengelola pengaturan aplikasi     |   17       |


# Nomor 2

Mampu mendemonstrasikan Class Diagram dari keseluruhan Use Case produk digital

![](https://gitlab.com/anismubarokah21/pbo/-/raw/main/class%20diagram/class_diagram_KAI.png)

# Nomor 3

Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle

Berikut adalah penjelasan penerapan prinsip SOLID Design Principle pada program yang saya buat :
1. Single Responsibility Principle (SRP), prinsip ini menyatakan bahwa sebuah kelas seharusnya hanya memiliki satu alasan untuk berubah. Artinya, kelas tersebut seharusnya hanya memiliki satu tanggung jawab.

Contoh kode:
```
<?php
use Slim\Http\Request;
use Slim\Http\Response;

require 'vendor/autoload.php';

class DatabaseConnection
{
    private $conn;

    public function __construct($dbConfig)
    {
        $this->conn = new mysqli($dbConfig['host'], $dbConfig['user'], $dbConfig['password'], $dbConfig['database']);
    }

    public function executeQuery($query)
    {
        return $this->conn->query($query);
    }
}

interface PemesananRepositoryInterface
{
    public function getAllPemesanan();
}

class PemesananRepository implements PemesananRepositoryInterface
{
    private $dbConnection;

    public function __construct(DatabaseConnection $dbConnection)
    {
        $this->dbConnection = $dbConnection;
    }

    public function getAllPemesanan()
    {
        $query = "SELECT * FROM Pemesanan";
        $result = $this->dbConnection->executeQuery($query);

        $data = [];
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
        }

        return $data;
    }
}

class PemesananService
{
    private $pemesananRepository;

    public function __construct(PemesananRepositoryInterface $pemesananRepository)
    {
        $this->pemesananRepository = $pemesananRepository;
    }

    public function getAllPemesanan()
    {
        return $this->pemesananRepository->getAllPemesanan();
    }
}

$app = new \Slim\App();

$dbConfig = [
    'user' => 'root',
    'password' => '',
    'host' => 'localhost',
    'database' => 'kaiaccess'
];

$dbConnection = new DatabaseConnection($dbConfig);
$pemesananRepository = new PemesananRepository($dbConnection);
$pemesananService = new PemesananService($pemesananRepository);

$app->get('/', function (Request $request, Response $response, $args) use ($pemesananService) {
    $data = $pemesananService->getAllPemesanan();

    $response->getBody()->write(json_encode($data));
    return $response->withHeader('Content-Type', 'application/json');
});

$app->run();
?>
```
Pada program diatas yang menerapkan Single Responsibility Principle ada pada :
```
class DatabaseConnection
{
    // mengelola koneksi ke database
    // ...
}

class PemesananRepository
{
    // mengelola akses data pemesanan
    // ...
}

class PemesananService
{
    // menangani logika bisnis terkait pemesanan
    // ...
}
```
Penjelasan :

Pada class DatabaseConnection bertanggung jawab hanya untuk mengelola koneksi ke database. Tanggung jawabnya adalah menyediakan metode untuk membuat koneksi dan menjalankan query.

Pada class PemesananRepository bertanggung jawab untuk mengakses data pemesanan dari database. Tanggung jawabnya adalah menyediakan metode untuk mengambil data pemesanan dari database.

Pada class PemesananService bertanggung jawab untuk menangani logika bisnis terkait pemesanan. Tanggung jawabnya adalah menyediakan metode untuk mengambil semua data pemesanan dan melakukan operasi bisnis lainnya terkait pemesanan.


2. Open-Closed Principle (OCP), prinsip ini menyatakan bahwa kode seharusnya terbuka untuk perluasan (open for extension) namun tertutup untuk modifikasi (closed for modification).

Contoh kode:
```
interface PemesananRepositoryInterface
{
    public function getAllPemesanan();
}

class PemesananRepository implements PemesananRepositoryInterface
{
   private $dbConnection;

    public function __construct(DatabaseConnection $dbConnection)
    {
        $this->dbConnection = $dbConnection;
    }

    public function getAllPemesanan()
    {
        $query = "SELECT * FROM Pemesanan";
        $result = $this->dbConnection->executeQuery($query);

        $data = [];
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
        }

        return $data;
    }
}
```
Penjelasan :

Dengan menggunakan interface, PemesananRepositoryInterface sebagai kontrak, kita dapat membuat implementasi baru seperti PemesananRepository yang memenuhi kontrak tersebut. Dengan demikian, kita dapat memperluas fungsionalitas sistem dengan menambahkan implementasi baru tanpa mengubah kode yang sudah ada dalam kelas PemesananService.


3. Liskov Substitution Principle (LSP), prinsip ini menyatakan bahwa objek dari kelas turunan harus dapat digunakan sebagai pengganti objek dari kelas induk tanpa mengganggu integritas program. Dalam program webservice yang telah saya buat, tidak terlihat langsung penggunaan prinsip LSP dikarenakan tidak ada hubungan pewarisan yang terlibat.

4. Interface Segregation Principle (ISP), prinsip ini menyatakan bahwa klien tidak boleh dipaksa bergantung pada antarmuka yang tidak mereka gunakan. Antarmuka seharusnya berfokus pada kebutuhan spesifik dari klien.

Contoh kode:
```
interface PemesananRepositoryInterface
{
    public function getAllPemesanan();
}
```
Penjelasan :

Prinsip ISP diterapkan dengan menggunakan PemesananRepositoryInterface. Antarmuka ini hanya menyediakan metode getAllPemesanan(), yang relevan dan diperlukan oleh PemesananService. Hal ini memastikan bahwa klien hanya bergantung pada metode yang sesuai dengan kebutuhannya, dan tidak dipaksa untuk bergantung pada metode lain yang tidak relevan.


5. Dependency Inversion Principle (DIP), prinsip ini menyatakan bahwa modul tingkat tinggi tidak boleh bergantung pada modul tingkat rendah, keduanya seharusnya bergantung pada abstraksi. Abstraksi tidak boleh bergantung pada detail, tetapi sebaliknya, detail seharusnya bergantung pada abstraksi.

Contoh kode:
```
class PemesananService
{
    private $pemesananRepository;

    public function __construct(PemesananRepositoryInterface $pemesananRepository)
    {
        $this->pemesananRepository = $pemesananRepository;
    }

    public function getAllPemesanan()
    {
        return $this->pemesananRepository->getAllPemesanan();
    }
}
```
Penjelasan :

Prinsip DIP diterapkan dengan menggunakan injeksi dependensi melalui konstruktor pada kelas PemesananService. PemesananService bergantung pada abstraksi PemesananRepositoryInterface daripada implementasi konkret PemesananRepository. Ini memungkinkan penggantian implementasi PemesananRepository dengan mudah tanpa mempengaruhi PemesananService, dan juga memungkinkan pengujian yang lebih mudah dengan menggunakan stub atau mock objek untuk PemesananRepository.

# Nomor 4

Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih

Berikut penejelasan design pattern yang saya pilih untuk program yang saya buat :

Dependency Injection (DI):
Pola desain Dependency Injection (DI) memungkinkan injeksi dependensi ke dalam objek yang membutuhkannya, sehingga ketergantungan antar objek dapat diatur dengan lebih fleksibel. Dalam kode di atas, konstruktor PemesananService menerima PemesananRepositoryInterface sebagai parameter, memungkinkan penggunaan berbagai implementasi PemesananRepository tanpa mengubah kode PemesananService. Ini memisahkan pembuatan objek dan manajemen ketergantungan dari logika bisnis.

Contoh kode:
```
class PemesananService
{
    private $pemesananRepository;

    public function __construct(PemesananRepositoryInterface $pemesananRepository)
    {
        $this->pemesananRepository = $pemesananRepository;
    }

    public function getAllPemesanan()
    {
        return $this->pemesananRepository->getAllPemesanan();
    }
}
```

Repository Pattern, pola desain Repository digunakan untuk memisahkan logika bisnis dari logika akses data. Dalam kode di atas, PemesananRepository bertindak sebagai penghubung antara PemesananService (logika bisnis) dan penyimpanan data (database). PemesananRepository menyediakan metode untuk mengambil dan memanipulasi data pemesanan. Dengan menggunakan pola desain Repository, logika bisnis tidak perlu tahu detail implementasi penyimpanan data, sehingga memungkinkan fleksibilitas dan penggantian implementasi penyimpanan dengan mudah.

```
interface PemesananRepositoryInterface
{
    public function getAllPemesanan();
}

class PemesananRepository implements PemesananRepositoryInterface
{
    private $dbConnection;

    public function __construct(DatabaseConnection $dbConnection)
    {
        $this->dbConnection = $dbConnection;
    }

    public function getAllPemesanan()
    {
        $query = "SELECT * FROM Pemesanan";
        $result = $this->dbConnection->executeQuery($query);

        $data = [];
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
        }

        return $data;
    }
}
```
# Nomor 5

Mampu menunjukkan dan menjelaskan konektivitas ke database

konektivitas ke database
![](https://gitlab.com/anismubarokah21/uaspbo/-/raw/main/ss/KoneksiDatabase.PNG)
Penjelasan :

Pada bagian awal kode, terdapat deklarasi variabel yang akan digunakan untuk koneksi ke database $hostname menyimpan nama host server database, dalam contoh ini adalah "localhost".  $username menyimpan nama pengguna untuk mengakses database, dalam koneksi saya adalah "root". $password menyimpan kata sandi untuk mengakses database, dalam koneksi database saya passwordnya kosong. $db_name menyimpan nama database yang akan digunakan, dalam koneksi database saya namanya adalah "kaiaccess" karena nama database saya dalam localhost ialah "kaiaccess". Pada bagian selanjutnya, terdapat fungsi mysqli_connect() yang digunakan untuk melakukan koneksi ke database MySQL. Fungsi ini menerima parameter berupa $hostname, $username, $password, dan $db_name. Jika koneksi berhasil, variabel $koneksi akan menyimpan objek koneksi yang akan digunakan untuk mengakses database. Jika koneksi gagal, fungsi die() akan membatalkan eksekusi program dan menampilkan pesan kesalahan koneksi. Saat koneksi database telah berhasil maka pada saat dijalankan di web tidak akan muncul pesan apapun (kosong).

# Nomor 6

Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya 

Pembuatan Web Service
```
<?php
use Slim\Http\Request;
use Slim\Http\Response;

require 'vendor/autoload.php';

class DatabaseConnection
{
    private $conn;

    public function __construct($dbConfig)
    {
        $this->conn = new mysqli($dbConfig['host'], $dbConfig['user'], $dbConfig['password'], $dbConfig['database']);
    }

    public function executeQuery($query)
    {
        return $this->conn->query($query);
    }
}

interface PemesananRepositoryInterface
{
    public function getAllPemesanan();
}

class PemesananRepository implements PemesananRepositoryInterface
{
    private $dbConnection;

    public function __construct(DatabaseConnection $dbConnection)
    {
        $this->dbConnection = $dbConnection;
    }

    public function getAllPemesanan()
    {
        $query = "SELECT * FROM Pemesanan";
        $result = $this->dbConnection->executeQuery($query);

        $data = [];
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
        }

        return $data;
    }
}

class PemesananService
{
    private $pemesananRepository;

    public function __construct(PemesananRepositoryInterface $pemesananRepository)
    {
        $this->pemesananRepository = $pemesananRepository;
    }

    public function getAllPemesanan()
    {
        return $this->pemesananRepository->getAllPemesanan();
    }
}

$app = new \Slim\App();

$dbConfig = [
    'user' => 'root',
    'password' => '',
    'host' => 'localhost',
    'database' => 'kaiaccess'
];

$dbConnection = new DatabaseConnection($dbConfig);
$pemesananRepository = new PemesananRepository($dbConnection);
$pemesananService = new PemesananService($pemesananRepository);

$app->get('/', function (Request $request, Response $response, $args) use ($pemesananService) {
    $data = $pemesananService->getAllPemesanan();

    $response->getBody()->write(json_encode($data));
    return $response->withHeader('Content-Type', 'application/json');
});

$app->run();
?>
```

Penjelasan pembuatan web service :
1. Web Service diatas dirancang menggunakan bahasa PHP framework Slim untuk membangun sebuah web dengan mudah dan ringan. Framework ini menyediakan kemudahan dalam mengatur rute (routes), middleware, dan penanganan permintaan HTTP. Sebelum saya membuat web service ini langkah awal yang dilakukan ialah menginstall Slim dan composer JSON agar web service dapat dijalankan dan menghasilkan output JSON. 
2. Membuat program, Pada baris $app = new \Slim\App();, objek Slim dengan nama $app diinisialisasi. Objek ini akan digunakan untuk mengkonfigurasi dan menjalankan web service. 
3. kemudian mengkonfigurasi koneksi database, dalam kode terdapat variabel $dbConfig yang berisi informasi koneksi ke database MySQL.
Informasi ini meliputi host, username, password, dan nama database yang digunakan Variabel ini akan digunakan untuk membuat objek DatabaseConnection. Sebelumnya saya telah membuat database terlebih dahulu dengan nama KAIACCESS di localhost. 
4. Membuat Objek DatabaseConnection, kelas DatabaseConnection adalah kelas yang bertanggung jawab untuk melakukan koneksi ke database MySQL menggunakan ekstensi MySQLi. Dalam konstruktor kelas DatabaseConnection, koneksi database MySQLi diinisialisasi dengan menggunakan informasi yang diberikan dalam variabel $dbConfig. Metode executeQuery() digunakan untuk menjalankan query pada database dan mengembalikan hasilnya. 
5. Membuat Interface PemesananRepositoryInterface, Interface PemesananRepositoryInterface didefinisikan dengan satu metode yaitu getAllPemesanan(). Interface ini akan menjadi kontrak yang harus diikuti oleh implementasi kelas PemesananRepository.
6. Membuat class PemesananRepository, class PemesananRepository mengimplementasikan interface PemesananRepositoryInterface.Konstruktor PemesananRepository menerima objek DatabaseConnection sebagai dependensi. Metode getAllPemesanan() dalam kelas ini mengambil semua data pemesanan dari tabel "Pemesanan" menggunakan objek DatabaseConnection. Hasil query akan dikonversi menjadi array asosiatif dan dikembalikan sebagai hasil. 
7. Membuat Kelas PemesananService, Kelas PemesananService bertanggung jawab untuk menyediakan layanan (service) terkait pemesanan.Konstruktor PemesananService menerima objek PemesananRepositoryInterface sebagai dependensi.Metode getAllPemesanan() dalam kelas ini menggunakan PemesananRepositoryInterface untuk mendapatkan semua data pemesanan.
8. Mengatur Route dan Handler, Terdapat route / yang menangani permintaan GET dengan menggunakan metode $app->get(). Metode get() menerima tiga parameter: path, callback handler, dan nama rute (opsional). Callback handler akan dipanggil ketika permintaan GET diterima pada route yang sesuai. Dalam callback handler tersebut, objek $pemesananService digunakan untuk memanggil metode getAllPemesanan() dan mendapatkan data pemesanan. Data pemesanan akan dikonversi menjadi format JSON menggunakan json_encode(). Hasil JSON akan dikirim sebagai respons dengan tipe konten application/json.
9. Menjalankan Aplikasi dengan memanggil metode $app->run(), web service akan dijalankan. Metode run() akan memproses permintaan masuk dan menangani rute yang sesuai dengan permintaan tersebut.

Operasi CRUD 

[Program Pemesanan](https://gitlab.com/anismubarokah21/uaspbo/-/blob/main/Project/Pemesanan.java)

Berikut adalah penjelasan mengenai Operasi CRUD dalam program Pemesanan Tiket :
1. Create (Simpan Data), Saat tombol "Simpan" (jButton_simpan) ditekan, akan dipanggil metode jButton_simpanActionPerformed(evt) untuk menyimpan data ke database. Metode ini mengambil nilai input dari komponen-komponen GUI seperti jTextField_nama, jTextField_nik, jComboBox_kelas, jTextField_SA, jTextField_ST, dan jTextField_harga. Menggunakan objek koneksi Koneksi.getConnection() untuk mendapatkan koneksi ke database. Membuat objek PreparedStatement dan mengeksekusi pernyataan INSERT untuk menyimpan data ke tabel "Pemesanan". Setelah berhasil disimpan, menampilkan dialog pesan sukses dan memanggil metode TampilData() untuk memperbarui tampilan tabel.
![](https://gitlab.com/anismubarokah21/uaspbo/-/raw/main/ss/Simpan.PNG)

2. Read (Tampil Data), Saat aplikasi dijalankan, konstruktor Pemesanan() akan dipanggil. Metode konstruktor Pemesanan() akan memanggil metode TampilData() untuk menampilkan data dari tabel "Pemesanan" ke dalam jTable_Pemesanan. Metode TampilData() akan mengambil data dari tabel "Pemesanan" menggunakan pernyataan SELECT dan mengisi model tabel pemesanan dengan data tersebut. Data dari res akan ditambahkan ke pemesanan menggunakan pemesanan.addRow(new Object[]{...}).
![](https://gitlab.com/anismubarokah21/uaspbo/-/raw/main/ss/read.PNG)


3. Update (Edit Data), Saat tombol "Edit" (jButton_edit) ditekan, akan dipanggil metode jButton_editActionPerformed(evt) untuk mengedit data yang dipilih dari tabel. Metode ini mengambil nilai input dari komponen-komponen GUI dan data yang dipilih dari tabel. Menggunakan objek koneksi Koneksi.getConnection() untuk mendapatkan koneksi ke database. Membuat objek PreparedStatement dan mengeksekusi pernyataan UPDATE untuk mengubah data dalam tabel "Pemesanan". Setelah berhasil diubah, menampilkan dialog pesan sukses dan memperbarui nilai yang diperbarui dalam model tabel pemesanan.
![](https://gitlab.com/anismubarokah21/uaspbo/-/raw/main/ss/edit.PNG)
![](https://gitlab.com/anismubarokah21/uaspbo/-/raw/main/ss/dATABASEEDIT.PNG)

4. Delete (Hapus Data), Saat tombol "Hapus" (jButton_hapus) ditekan, akan dipanggil metode jButton_hapusActionPerformed(evt) untuk menghapus data yang dipilih dari tabel. Metode ini mengambil nilai input dari komponen jTextField_nik dan data yang dipilih dari tabel. Menggunakan objek koneksi Koneksi.getConnection() untuk mendapatkan koneksi ke database. Membuat objek PreparedStatement dan mengeksekusi pernyataan DELETE untuk menghapus data dalam tabel "Pemesanan". Menampilkan dialog konfirmasi sebelum menghapus data. Setelah berhasil dihapus, menampilkan dialog pesan sukses, memanggil TampilData() untuk memperbarui tampilan tabel, dan mengosongkan input pada komponen-komponen GUI.
![](https://gitlab.com/anismubarokah21/uaspbo/-/raw/main/ss/hapus1.PNG)
![](https://gitlab.com/anismubarokah21/uaspbo/-/raw/main/ss/hapus2.PNG)


# Nomor 7

Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital

[GUI](https://gitlab.com/anismubarokah21/uaspbo/-/blob/main/Project/Login.java)

Pada GUI ini terdapat komponen-komponen berikut:

1. jLabel2: Label yang menampilkan gambar kai access pada panel atas.
2. btBeranda, btLogin, btRegister: Tombol-tombol yang berada di sebelah kanan gambar.
3. jLabel3: Label yang menampilkan teks "Username".
4. jLabel4: Label yang menampilkan teks "Password".
5. tfUsername: Text field yang digunakan untuk menginputkan username.
6. tfPassword: Text field yang digunakan untuk menginputkan password.
7. bLogin: Tombol "Login" untuk melakukan proses login.
8. btRegis: Tombol "Register" untuk melakukan proses registrasi.
9. btLupapass: Tombol "Lupa Pass" untuk melakukan proses pemulihan password.
10. jLabel1: Label yang menampilkan teks "Login" sebagai judul panel login.

![](https://gitlab.com/anismubarokah21/uaspbo/-/raw/main/ss/INTERFACE1.PNG)


# Nomor 8

Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital

[HTTP Connection](https://gitlab.com/anismubarokah21/uaspbo/-/blob/main/Project/WebServiceClient.java)

Penjelasan mengenai HTTP Connection :
1. Import Package, pada bagian awal kode, terdapat deklarasi package yang akan digunakan yaitu package kai;. Package ini akan memuat kelas-kelas yang digunakan dalam implementasi HTTP connection.
2. Base URL, pada bagian konstanta BASE_URL, diinisialisasikan URL dasar dari web service yang akan diakses. Dalam contoh ini, digunakan http://localhost/KAIACCESS/ sebagai base URL, URL ini menunjukkan alamat server yang akan dihubungi.
3. sendGetRequest() Method, fungsi sendGetRequest() merupakan metode untuk mengirim permintaan GET ke web service. Metode ini menerima endpoint sebagai parameter yang digunakan untuk membangun URL lengkap yang akan dihubungi. 
4. Main() Method, main() Method adalah metode utama yang menjalankan contoh penggunaan dari sendGetRequest(). Pada contoh ini, contoh penggunaan menggunakan sendGetRequest("index.php") untuk mengakses endpoint index.php pada web service. Kode ini adalah contoh bagaimana memanggil metode sendGetRequest() dengan parameter endpoint tertentu.
5. Pada akhir kode, setelah memanggil sendGetRequest(), hasil respons akan ditampilkan pada konsol melalui System.out.println().

# Nomor 9

Mampu Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube

[Link Youtube](https://youtu.be/Ab-vkwlZGwA)

# Nomor 10

Bonus: Mendemonstrasikan penggunaan Machine Learning

