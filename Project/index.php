<?php
use Slim\Http\Request;
use Slim\Http\Response;

require 'vendor/autoload.php';

class DatabaseConnection
{
    private $conn;

    public function __construct($dbConfig)
    {
        $this->conn = new mysqli($dbConfig['host'], $dbConfig['user'], $dbConfig['password'], $dbConfig['database']);
    }

    public function executeQuery($query)
    {
        return $this->conn->query($query);
    }
}

interface PemesananRepositoryInterface
{
    public function getAllPemesanan();
}

class PemesananRepository implements PemesananRepositoryInterface
{
    private $dbConnection;

    public function __construct(DatabaseConnection $dbConnection)
    {
        $this->dbConnection = $dbConnection;
    }

    public function getAllPemesanan()
    {
        $query = "SELECT * FROM Pemesanan";
        $result = $this->dbConnection->executeQuery($query);

        $data = [];
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
        }

        return $data;
    }
}

class PemesananService
{
    private $pemesananRepository;

    public function __construct(PemesananRepositoryInterface $pemesananRepository)
    {
        $this->pemesananRepository = $pemesananRepository;
    }

    public function getAllPemesanan()
    {
        return $this->pemesananRepository->getAllPemesanan();
    }
}

$app = new \Slim\App();

$dbConfig = [
    'user' => 'root',
    'password' => '',
    'host' => 'localhost',
    'database' => 'kaiaccess'
];

$dbConnection = new DatabaseConnection($dbConfig);
$pemesananRepository = new PemesananRepository($dbConnection);
$pemesananService = new PemesananService($pemesananRepository);

$app->get('/', function (Request $request, Response $response, $args) use ($pemesananService) {
    $data = $pemesananService->getAllPemesanan();

    $response->getBody()->write(json_encode($data));
    return $response->withHeader('Content-Type', 'application/json');
});

$app->run();
?>